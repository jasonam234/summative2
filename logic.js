function generateEnemyShootTime() {
    let min = 1.0;
    let max = 2.0;
    let highlightedNumber = Math.random() * (max - min) + min;
    return highlightedNumber.toFixed(2);
};

function generateBoxTime() {
    let min = 1000;
    let max = 2000;
    let highlightedNumber = Math.random() * (max - min) + min;
    return highlightedNumber;
};

function createTrigger(){
    let btn = document.getElementById("start-button");
    btn.id = "trigger-button";
    btn.classList.add("hidden");

    setTimeout(function(){
        console.log("Button Created");
        btn.classList.remove("hidden");
        document.getElementById("player").src = "./assets/images/player_ready.png";
        document.getElementById("enemy").src = "./assets/images/enemy_ready.png";
        createdTime = Date.now();

        console.log(createdTime);

        btn.onclick = function(){
            let playerWin;
            let enemyWin;
            var clickedTime;
            var reactionTime; 
            clickedTime=Date.now();
            reactionTime=(clickedTime-createdTime)/1000;
            if(reactionTime.toFixed(2) < generateEnemyShootTime()){
                playerWin = parseInt(localStorage.getItem("playerScore")) + 1;
                console.log("win : " + playerWin);
                localStorage.setItem("playerScore", playerWin);
                console.log(localStorage.getItem("playerScore"));
                document.getElementById("enemy").src = "./assets/images/enemy_dead.png";
                currentStatus = gameStatus.start;
            }else if(reactionTime.toFixed(2) > generateEnemyShootTime()){
                enemyWin = parseInt(localStorage.getItem("enemyScore")) + 1;
                console.log("Enemy win : " + enemyWin);
                localStorage.setItem("enemyScore", enemyWin);
                document.getElementById("player").src = "./assets/images/player_dead.png";
                currentStatus = gameStatus.start;
            } 
            if(localStorage.getItem("playerScore") === "3"){
                document.getElementById("condition").src = "./assets/images/win.png";
                localStorage.removeItem("playerScore");
                localStorage.removeItem("enemyScore");
            }else if(localStorage.getItem("enemyScore") === "3"){
                document.getElementById("condition").src = "./assets/images/lose.png";
                localStorage.removeItem("playerScore");
                localStorage.removeItem("enemyScore");
            }
        };
    },generateBoxTime())
}


let start = document.getElementById("start-button");

let gameStatus = {
    start: 1,
    stop: 2
}

let currentStatus = gameStatus.stop;

start.addEventListener('click', function(){
    if(currentStatus === gameStatus.stop){
        startGame();
        // console.log(localStorage.getItem("score"));
        if(localStorage.getItem("playerScore") === null && localStorage.getItem("enemyScore") === null){
            localStorage.setItem("playerScore", 0);
            localStorage.setItem("enemyScore", 0);
            console.log(localStorage.getItem("score"));
        }
        createTrigger();
    }else{
        stopGame();
        let btn = document.getElementById("trigger-button");
        btn.remove();
        let createBtn = document.createElement("button");
        createBtn.id = "restart-button"
        createBtn.onclick = function(){
            location.reload();
        }
        document.getElementById("restart_section").appendChild(createBtn);
    }
})

function restart(){
    location.reload();
}

function stopGame() {
    currentStatus = gameStatus.stop;
    console.log("game stop");
}

function startGame(){
    currentStatus = gameStatus.start;
    console.log("game start");
}
